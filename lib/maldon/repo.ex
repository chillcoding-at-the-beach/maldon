defmodule Maldon.Repo do
  use Ecto.Repo, otp_app: :maldon

  @moduledoc """
  In memory repository
  """

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
  end
  #
  # # def all(MaldonWeb.User) do
  # #   [%MaldonWeb.User{id: "1", name: "Rémy Coutable", username: "rymai", password: "secure"}]
  # # end
  # def all(_module), do: []
  #
  # def get(module, id) do
  #   Enum.find all(module), fn map -> map.id == id end
  # end
  #
  # def get_by(module, params) do
  #   Enum.find all(module), fn map ->
  #     Enum.all?(params, fn {key, val} -> Map.get(map, key) == val end)
  #   end
  # end
end
