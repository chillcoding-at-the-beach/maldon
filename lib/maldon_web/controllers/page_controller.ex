defmodule MaldonWeb.PageController do
  use MaldonWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
